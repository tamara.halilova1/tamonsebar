import * as React from 'react';
import styles from './styles.module.scss';

interface IProps {
  sbNotificationText: string;
  sbNotificationMessage: string;
  sbComments: string;
  isOpen: (boolean) => void;
  closeModal: (e) => void;
  isRead: (e) => void;
}

const NotificationModal: React.FC<IProps> = ({ sbNotificationText, sbNotificationMessage, sbComments, isRead, isOpen, closeModal }) => {
  const [isReadChecked, setIsReadChecked] = React.useState(false);

  const handleIsRead = (e) => {
    setIsReadChecked(!isReadChecked);
    isRead(e);
  };

  return (
    <>
      <div className={styles["modal"]}>
        <div onClick={closeModal} className={styles["modal__overlay"]}>
          <div className={styles["modal__window"]}>

            <div className={styles["modal__header"]}>
              <h2 className={styles["modal__title"]}>
                {sbNotificationText}
              </h2>
            </div>

            <div dangerouslySetInnerHTML={{__html: sbNotificationMessage}} className={styles["modal__body"]}></div>

            <div className={styles["modal__footer"]}>
              <div className={styles["modal__comment"]}>
                <h3>Комментарий руководителя по ИПР:</h3>
                <p>{sbComments}</p>
              </div>

              <div className={styles["modal__agreement"]}>
                <label>
                  Ознакомлен
                  <input
                    type="checkbox"
                    checked={isReadChecked}
                    onChange={handleIsRead}
                  />
                </label>
              </div>
            </div>

            <button
              className={styles["modal__close-btn"]}
              onClick={() => isOpen(false)}
            >
              <img className={styles["modal__close-img"]} src="../../assets/BtnClose/btnClose.svg" alt="close" />
            </button>
          </div>
        </div>
      </div>
    </>
  )
}

export default NotificationModal;

import * as React from 'react'
import Api from '../../../api'
import { UserInfo } from '../../UserInfo/UserInfo'
import { Spiner } from '../../Spiner/Spiner';

export const PersonalInfoContainer = () => {
  const [userInfo, setUserInfo] = React.useState(null)
  // const [userDepartment, setUserDepartment] = React.useState(null)

    React.useEffect(() => {
        getUserInfo();
    }, [])

    console.log(userInfo);
    const getUserInfo = async () => {
        try {
            const currrentUser = await Api.getUserContext()

            try {
                const userInfo = await Api.getPersonalInfo(currrentUser)
                // const userDepartment = await Api.getDepartment(userInfo[0].departmentId)
                setUserInfo(userInfo);
                // setUserDepartment(userDepartment);
            } catch (error) {
                console.log('Get userInfo error', error)
            }
        } catch (error) {
            console.log('Get user userInfo error', error)
        }
    }
    if (userInfo === null) {
      return (<div style={{width: '290px', textAlign:'center'}}><Spiner /></div>

      )
    }else {
      return (
         <UserInfo userInfo={userInfo[0]}/>
   )
    }


}


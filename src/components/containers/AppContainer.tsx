import { WebPartContext } from "@microsoft/sp-webpart-base";
import * as React from "react";
import NotificationsContainer from "./NotificationsContainer";
import { sp } from "@pnp/sp";
import { PersonalInfoContainer } from "./PersonalInfoContainer/PersonalInfoContainer";
import styles from './AppContainer.module.scss';


interface IProps {
  description: string
}

export default (props: IProps) => {
  return (
    <div className={styles["main-container"]}>
      <div className={styles.cabinet}>
       <PersonalInfoContainer />
      </div>
      
      <div className={styles.notifications}>
        <h2>Профіль посади</h2>
        <NotificationsContainer/>
      </div>
      
      <div className={styles.roles}>
        <h2>Ролі у проектах</h2>
        <div>Ребята, компоненту таблицы продолжайте <strong>Здесь!</strong> :)</div>
      </div>
    </div>
  )
}

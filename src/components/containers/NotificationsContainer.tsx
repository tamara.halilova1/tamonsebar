import * as React from "react";
import Notification from "../Notification";
import Api from "../../api";
import NotificationModal from "../NotificationModal";
import styles from '../Notification/styles.module.scss'

const NotificationsContainer = () => {
  const [notifications, setNotifications] = React.useState(null);
  const [open, setOpen] = React.useState(false);

  const updateIsRead = async (id, value) => {
    try {
      await Api.updateIsRead(id, value);
    } catch (error) {
      console.log("Update notification error", error);
    }
  };

  const getNotifications = async () => {
    try {
      const currrentUser = await Api.getUserContext();
      
      try {
        const data = await Api.getNotification(currrentUser);
        
        data.forEach((elem) => {
          elem.isRead = (e) => {
            updateIsRead(data[0].Id, e.target.checked);
          };
          elem.isOpen = setOpen;
        });
        setNotifications(data);
      } catch (error) {
        console.log("Get notification error", error);
      }
    } catch (error) {
      console.log("Get user context error", error);
    }
  };

  React.useEffect(() => {
    getNotifications();
  }, []);

  const openModal = () => {setOpen(true);};
  const closeModal = (e) => {
    if (e.target === e.currentTarget) {
      setOpen(false);
    }
  };

  console.log(notifications);

  return (
    <>
      <div className={styles.containner}>
        {notifications &&  notifications.map((_,i,array)=><Notification number={i} {...array[i]} openModal={openModal} notification={array[i]} />)} 
        {(open && notifications) && <NotificationModal {...notifications[0]} closeModal={closeModal} />}
        <button type='button' className={styles.downBtn}></button>
      </div>
    </>
  );
};

export default NotificationsContainer;

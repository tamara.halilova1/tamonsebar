import * as React from 'react';
import styles from './styles.module.scss';

interface IProps {
  userInfo: any;
}

export const UserInfo: React.FC<IProps> = ({userInfo:{Title, Position, DepartmentId, sbEmail, sbPosition, sbWorkPhone, sbWorkCity, sbEmployeePhotoURL}}) => {

    return (
        <div className={styles["info"]}>
           {sbEmployeePhotoURL === null ? <div className={styles["info__no-img"]}>{Title.slice(0,1)}</div> : <img className={styles["info__img"]} src={sbEmployeePhotoURL}></img>}
          <h3 className={styles["info__title"]}>{Title}</h3>
          <p className={styles["info__position"]}>{Position}</p>
          <div className={styles["line"]}></div>
          <p className={styles["info__department"]}>Department: {DepartmentId}</p> {/* Виводить інфо департаменту після запросу(запрос не встиг зробити)*/}
          <p className={styles["info__additional"]}>e-mail:<span>{sbEmail}</span></p>
          <p className={styles["info__additional"]}>mobile:<span>{sbWorkPhone}</span></p>
          <div className={styles["line"]}></div>
        </div>
    )
}


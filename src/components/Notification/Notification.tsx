import * as React from "react";
import styles from "./styles.module.scss";
import BellSvg from "../../assets/BellSvg";
import ArrowSvg from "../../assets/ArrowSvg";

interface IProps {
  openModal: () => void;
  number: number;
  notification: {
    sbNotificationText: string;
  };
}

export default function Notification(props: IProps) {

  const countMessage = ()=> (!props.number)? '1.0.' : (props.number+10)/10 + '.';

  return (
    <div className={styles['notification-wrapper']}>
      <div className={styles["containner-info"]}>
        <BellSvg />
  <div className={styles["containner-info__messagesCount"]}>{countMessage()}</div>
        <div className={styles["containner-info__text"]}>
          {props.notification.sbNotificationText}
        </div>
      </div>
      <div className={styles["containner-more"]}>
        <button
          onClick={props.openModal}
          className={styles["containner-more__button"]}
        >
          Подробнее{" "}
        </button>
        <ArrowSvg />
      </div>

    </div>
  );
}

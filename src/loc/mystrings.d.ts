declare interface IProduct2WebPartWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'Product2WebPartWebPartStrings' {
  const strings: IProduct2WebPartWebPartStrings;
  export = strings;
}

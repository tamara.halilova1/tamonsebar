import { sp } from "@pnp/sp"
import "@pnp/sp/webs"
import "@pnp/sp/lists/web"
import "@pnp/sp/items/list"
import "@pnp/sp/site-users/web"

import lists from '../constants/lists'

export default class Api {
    static async getUserContext() {
        //Получаем контекст текущего юзера
        return await sp.web.currentUser.get()
    }
    static async getNotification(currentUser) {
        //Делаем запрос к списку с фильтрацие по юзеру и по полю isRead
        return await sp
            .web
            .lists
            .getByTitle(lists.notifications)
            .items
            .select('Id', 'sbNotificationType', 'isRead', 'sbNotificationMessage', 'sbNotificationText')
            .filter(`sbPersonId eq ${currentUser.Id} and isRead eq 0`)
            .get()
    }
    static async updateIsRead(id, value) {
        return await sp
            .web
            .lists
            .getByTitle(lists.notifications)
            .items
            .getById(id)
            .update({isRead: value});
    }

    static async getPersonalInfo(currentUser) {
      //Запрос до списку контактів за конкретним даними для конкретного юзера
        return await sp
            .web
            .lists
            .getByTitle(lists.contacts)
            .items
            .select('Title', 'Position', 'DepartmentId', 'sbEmail', 'sbPosition' , 'sbWorkPhone', 'sbWorkCity', 'sbEmployeePhotoURL')
            .filter(`sbPersonStringId eq ${currentUser.Id}`)
            .get()
    }

    static async getDepartment(userDepartmentId) {
      //Запрос до списку департаментів за конкретним депатраментом для конкретного юзера
        return await sp
            .web
            .lists
            .getByTitle(lists.departments)
            .items
            .select('Title')
            .filter(`Id eq ${userDepartmentId}`)
            .get()
    }
}
